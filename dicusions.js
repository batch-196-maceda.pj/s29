// db.products.insertMany(
//     [
//     {
//         "name":"Iphone X",
//         "price":30000,
//         "isActive": true
//      },
//      {"name":"Samsung Galaxy s21",
//         "price":51000,
//         "isActive": true
//      },
//      {
//          "name":"Razer Blachshark v2x",
//         "price":28000,
//         "isActive": false
//       },
//       {
//           "name":"RAKK Gaming Mouse",
//         "price":1800,
//         "isActive": true
//       },
//       {
//        "name":"Razer Mechanical Keyoard",
//         "price":4000,
//         "isActive": true
//       }
//     ])
      
      
 //query operators 
      //allows us to expand our queries and define conditions
      //insted of just looking specific values
      
 //$gt,$lt,$gte, $lte
      
 //$gt-grater than
// db.products.find({price:{$gt:30000}})
      
      
//$lt      
 // db.products.find({price:{$lt:30000}})
      
      
//$gte
      //db.products.find({price:{$gte:30000}})
      
//$lte
  //   db.products.find({price:{$lte:2800}})    
  
  
// db.newUsers.insertMany(
//     [
//         {
//         "firstname":"Mary Jane",
//         "lastname":"Watson",
//         "email":"mjtiger@gmail.com",
//         "password":"tigerjackpot15",
//         "isAdmin":false
//         },
//         {"firstname":"Gwen",
//         "lastname":"Stacy",
//         "email":"stacyTech@gmail.com",
//         "password":"staceTech1991",
//         "isAdmin": true
//         },
//         {"firstname":"Peter",
//         "lastname":"Parker",
//         "email":"peterWebDev@gmail.com",
//         "password":"webdeveloperPeter",
//         "isAdmin":true
//         },
//         {"firstname":"Jona",
//         "lastname":"Hex",
//         "email":"jjjameson@gmail.com",
//         "password":"spideyisamenace",
//         "isAdmin":false
//         },
//         {"firstname":"Otto",
//         "lastname":"Octavius",
//         "email":"ottoOctavius@gmail.com",
//         "password":"docOck15",
//         "isAdmin":true
//         }
//    ])


//$regex - query operator whic will find docunebts wich will match the charaters pattern of the characters we are looking for

//db.newUsers.find({firstname:{$regex: 'o'}})//key sensetive, case sensitive specific character format.

//$options
//db.newUsers.find({firstname:{$regex: 'O',$options: '$i'}})


//you can also find documents with partial matches
//db.products.find({name:{$regex: 'phone',$options: '$i'}})

//find users whose email have the word web
//db.newUsers.find({email:{$regex: 'web',$options: '$i'}})

//db.products.find({name:{$regex: 'razer',$options: '$i'}})
//db.products.find({name:{$regex: 'rakk',$options: '$i'}})

//$or $and- logical operators-works almost the same was as they do in JS 
//$or- allow us to have a logical operation to find a document which can satisfy atleast 1 of our conditions
//meaning meeting any of the set condition will include the document as a result of the query


//db.products.find({$or:[{name:{$regex:'x',$options: '$i'}},{price:{$lte:10000}}]})

//db.products.find({$or:[{name:{$regex:'x',$options: '$i'}},{price:{$gte:30000}}]})

//$and- look or find for documents that satisfies both condition

//db.products.find({$and:[{name:{$regex:'razer',$options: '$i'}},{price:{$gte:3000}}]})

//db.products.find({$and:[{name:{$regex:'x',$options: '$i'}},{price:{$gte:30000}}]})

//db.newUsers.find({$and:[{lastname:{$regex:'w',$options: '$i'}},{isAdmin:false}]})
//db.newUsers.find({$and:[{firsname:{$regex:'a',$options: '$i'}},{isAdmin:true}]})

//Field Projection
//expanded db.newUsers.find({all query"empty"},{_id:0,password:0}"pojection")
//in field projection -0 means hide, 1 means show


db.newUsers.find({},{_id:0,password:0})

db.newUsers.find({isAdmin:true},{_id:0,email:1})
//at default id will be shown unless istructed to hide,
db.newUsers.find({isAdmin:false},{firstname:1,lastname:1,_id:0})

//you can modiff which one to show and whoich 1 is to hide! lik so!
db.products.find({price:{$gte:10000}},{_id:0,isActive:0})