db.newCourses.insertMany(
    [  
    {
        "name":"HTML basics",
        "price":20000,
        "isActive":true,
        "instructors":"Sir Alvin"
        },
        {"name":"CSS 101 + Flexbox",
        "price":21000,
        "isActive": true,
        "instructors":"Sir Alvin"
        },
        {"name":"Javascript101",
        "price":32000,
        "isActive":true,
        "instructors": "Ma'am Tine"
        },
        {"name":"Git101, IDE and CLI",
        "price":19000,
        "isActive":false,
        "instructors":"Ma'am Tine"
        },
        {"name":"React.JS",
        "price":25000,
        "isActive":true,
        "instructors":"Ma'am Miah"
        }
    ])

db.newCourses.find({$and:[{instructors:{$regex:'Sir Alvin',$options: '$i'}},{price:{$gte:20000}},{_id:0,name:1,price:1}]})


 db.newCourses.find({$and:[{instructors:"Ma'am Tine"},{isActive:false}]},{_id:0,name:1,price:1})


db.newCourses.find({$and:[{name:{$regex:'r',$options: '$i'}},{price:{$lte:25000}}]})


db.newCourses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})


db.newCourses.deleteMany({"price":{$gte:25000}})


